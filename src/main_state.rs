use std;
use input::Input;
use game_state::GameState;
use game_services::GameServices;
use point::Point;
use buffer::Buffer;
use game_common::*;
use game_over_state::GameOverState;


type Body = std::collections::LinkedList<Point>;

#[derive(Debug, Default)]
pub struct MainState {
    body: Body,
    tail: Point,
    food: Point,
    direction: Point,
    user_input: Point,
    food_till_victory: i8,
    game_over: bool,
    victory: bool,
}

impl GameState for MainState {
    fn update(&mut self, input: &Input, game_services: &mut GameServices) {
        if self.game_over {
            game_services.set_next_state(Box::new(GameOverState{}));
            return;
        }

        if self.victory {
            game_services.set_next_state(Box::new(GameOverState{}));
            return;
        }        

        let pt = input.axes();

        if !pt.is_zero() {
            self.user_input = pt.clone();
        }
    }

    fn fixed_update(&mut self) {
        if self.game_over || self.victory {
            return;
        }

        if !self.user_input.is_zero() {
            if self.direction.is_horizontal() && self.user_input.is_vertical() {
                self.direction = Point {
                    x: 0,
                    y: self.user_input.y,
                }
            }

            if self.direction.is_vertical() && self.user_input.is_horizontal() {
                self.direction = Point {
                    x: self.user_input.x,
                    y: 0,
                }
            }

            self.user_input = Default::default();
        }


        // Move the body
        let mut pt = self.body.front().expect("Head is missing").clone();
        pt.x += self.direction.x;
        pt.y += self.direction.y;

        let consuming_food = pt == self.food;

        // Check collisions
        if self.check_collisions(&pt) {
            self.game_over = true;
            return;
        }

        self.body.push_front(pt);

        if !consuming_food {
            self.tail = self.body.pop_back().expect("Tail is missing");
        } else {
            self.food_till_victory -= 1;
            if self.food_till_victory > 0 {
                self.food = Self::generate_food(&self.body);
            } else {
                self.victory = true;
            }
        }
    }

    fn draw(&self, buffer: &mut Buffer) {
        // Erase the tail
        buffer.set_point(&self.tail, BLACK);

        // Draw the body
        for pt in &self.body {
            buffer.set_point(&pt, WHITE);
        }

        // Draw the food
        buffer.set_point(&self.food, WHITE);
    }
}

impl MainState {
    pub fn new() -> Self {
        use std::default::Default;

        let body = {
            use std::collections::LinkedList;

            let body_len = 3;
            let mut body = LinkedList::new();
            let x = WIDTH / 2;
            let y = HEIGHT / 2 - body_len / 2;
            for i in 0..body_len {
                let pt = Point{ x, y: y + i };
                body.push_back(pt);
            }

            body
        };

        let food = Self::generate_food(&body);

        Self {
            body,
            food,
            direction: Point{ x:0, y:-1 },
            game_over: false, 
            food_till_victory: ((WIDTH as i16 * HEIGHT as i16) / 4 * 2) as i8,
            ..Default::default()
        }
    }

    fn check_border_collision(pt: &Point) -> bool {
        (pt.x < 0) || (pt.x >= WIDTH) || (pt.y < 0) || (pt.y >= HEIGHT)
    }

    fn check_body_collision(pt: &Point, body: &Body) -> bool {
        body.iter().any(|p|{
            *p == *pt
        })
    }

    fn check_collisions(&self, pt: &Point) -> bool {
        // Borders
        if Self::check_border_collision(pt) {
            return true;
        }

        // Body
        if Self::check_body_collision(pt, &self.body) {
            return true;
        }

        // Obstacles

        false
    }

    fn generate_food(body: &Body) -> Point {
        use rand::{thread_rng, Rng};
        let mut rng = thread_rng();

        loop {
            let x = rng.gen_range(0i8, WIDTH);
            let y = rng.gen_range(0i8, HEIGHT);

            let food = Point{x, y};
            if !Self::check_body_collision(&food, body) {
                return food;
            }
        }
    }
}
