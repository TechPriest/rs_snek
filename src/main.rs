extern crate minifb;
extern crate time;
extern crate rand;

mod point;
mod buffer;
mod game_common;
mod input;
mod keyboard_input;
mod game_services;
mod game_state;
mod main_state;
mod game_over_state;
mod game;


fn main() {
    use game::Game;

    let mut game = Game::new();
    game.run();
}
