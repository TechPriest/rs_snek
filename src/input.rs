use point::Point;

pub trait Input {
    fn axes(&self) -> &Point;
    fn quit(&self) -> bool;
    fn pause(&self) -> bool;
}