use input::Input;
use buffer::Buffer;
use game_services::GameServices;


pub trait GameState {
    fn update(&mut self, _input: &Input, _game_services: &mut GameServices) {}
    fn fixed_update(&mut self) {}
    fn draw_once(&self, _buffer: &mut Buffer) {}
    fn draw(&self, _buffer: &mut Buffer) {}
}
