use std;
use time;

use buffer::Buffer;
use input::Input;
use keyboard_input::KeyboardInput;

use minifb::{Scale, Window, WindowOptions};
use game_services::GameServices;
use game_state::GameState;
use game_common::*;


const TITLE: &'static str = "RS Snek";

pub struct Game {
    state: Box<GameState>,
    input: KeyboardInput,
    buffer: Buffer,
    window: Window,
}

impl Game {
    pub fn new() -> Self {
        use main_state::MainState;

        let window = Window::new(
            TITLE,
            WIDTH as usize,
            HEIGHT as usize,
            WindowOptions {
                resize: false,
                scale: Scale::X32,
                ..WindowOptions::default()
            },
        ).unwrap();

        Self {
            state: Box::new(MainState::new()),
            input: KeyboardInput::new(),
            buffer: Buffer::new(WIDTH as u32, HEIGHT as u32),
            window,
        }
    }

    fn update(&mut self) {
        self.input.update(self.window.get_keys());

        #[derive(Default)]
        struct Services {
            pub status: Option<String>,
            pub next_state: Option<Box<GameState>>,
        }

        impl GameServices for Services {
            fn set_status_text(&mut self, text: &str) {
                self.status = Some(text.into());
            }

            fn set_next_state(&mut self, state: Box<GameState>) {
                self.next_state = Some(state);
            }
        }

        let mut services: Services = std::default::Default::default();
        self.state.update(&self.input, &mut services);

        if let Some(status) = services.status {
            let title = format!("{0}: {1}", TITLE, status);
            self.window.set_title(&title);
        }

        if let Some(state) = services.next_state {
            self.input.reset();
            self.state = state;
            self.buffer.fill(BLACK);
            self.state.draw_once(&mut self.buffer);
        }
    }

    fn fixed_update(&mut self) {
        self.state.fixed_update();
    }

    fn draw(&mut self) {
        self.state.draw(&mut self.buffer);
    }

    pub fn run(&mut self) {
        const UPDATE_TIME: f64 = 1f64 / 3f64;
        let mut time_since_update = 0f64;

        self.state.draw_once(&mut self.buffer);

        while self.window.is_open() && !self.input.quit() {
            const IDEAL_FRAME_TIME: f64 = 1f64 / FPS as f64;
            let frame_start_time = time::precise_time_s();

            self.update();

            if time_since_update >= UPDATE_TIME {
                self.fixed_update();
                self.draw();

                time_since_update = 0f64;
            }

            self.window.update_with_buffer(self.buffer.data()).unwrap();

            let frame_end_time = time::precise_time_s();
            let frame_time = frame_end_time - frame_start_time;
            time_since_update += frame_time;

            if frame_time < IDEAL_FRAME_TIME {
                let delay = std::time::Duration::from_millis( (IDEAL_FRAME_TIME - frame_time) as u64 );
                std::thread::sleep(delay);
            }
        }
    }
}
