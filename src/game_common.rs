pub const WIDTH: i8 = 10;
pub const HEIGHT: i8 = 20;
pub const FPS: i32 = 30;
pub const BLACK: u32 = 0;
pub const WHITE: u32 = 0xffffff;