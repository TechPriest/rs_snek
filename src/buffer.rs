use point::Point;


#[derive(Debug, Default, Clone)]
pub struct Buffer {
    width: u32,
    height: u32,
    data: Vec<u32>,
}


impl Buffer {
    pub fn new(width: u32, height: u32) -> Self {
        let size = width as usize * height as usize;
        let data = vec![0u32; size];
        Self {
            width,
            height,
            data,
        }
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn data(&self) -> &[u32] {
        &self.data[..]
    }

    pub fn set_point(&mut self, pt: &Point, clr: u32) {
        let offset = (pt.y  as u32 * self.width() + pt.x as u32) as usize;
        self.data[offset] = clr;
    }

    pub fn fill(&mut self, clr: u32) {
        let size = self.width() as usize * self.height() as usize;
        for offset in 0 .. size {
            self.data[offset] = clr;
        }
    }
}