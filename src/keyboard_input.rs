use std;
use minifb::Key;
use input::Input;
use point::Point;


#[derive(Debug, Default)]
pub struct KeyboardInput {
    axes: Point,
    quit: bool,
    pause: bool,
}

impl KeyboardInput {
    pub fn new() -> Self {
        std::default::Default::default()
    }

    pub fn update(&mut self, keys: Option<Vec<Key>>) {
        *self = std::default::Default::default();

        if let Some(keys) = keys {
            let mut pt: Point = Default::default();
            for k in keys {
                match k {
                    Key::Left => {
                        pt.x -= 1;
                    },
                    Key::Right => {
                        pt.x += 1;
                    }
                    Key::Up => {
                        pt.y -= 1;
                    },
                    Key::Down => {
                        pt.y += 1;
                    },
                    Key::Escape => {
                        self.quit = true;
                    },
                    Key::P => {
                        self.pause = true;
                    },
                    _ => (),
                }
            }

            self.axes = pt;
        }
    }

    pub fn reset(&mut self) {
        *self = std::default::Default::default();
    }
}

impl Input for KeyboardInput {
    fn axes(&self) -> &Point {
        &self.axes
    }

    fn quit(&self) -> bool {
        self.quit
    }

    fn pause(&self) -> bool {
        self.pause
    }
}
