use std;

#[derive(Debug, Default, Clone)]
pub struct Point {
    pub x: i8,
    pub y: i8,
}

impl Point {
    pub fn is_zero(&self) -> bool {
        (self.x == 0) && (self.y == 0)
    }

    pub fn is_horizontal(&self) -> bool {
        self.x != 0
    }

    pub fn is_vertical(&self) -> bool {
        self.y != 0
    }
}

impl std::cmp::PartialEq for Point {
    fn eq(&self, b: &Self) -> bool {
        (self.x == b.x) && (self.y == b.y)
    }
}