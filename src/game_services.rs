use game_state::GameState;

pub trait GameServices {
    fn set_status_text(&mut self, text: &str);
    fn set_next_state(&mut self, state: Box<GameState>);
}
