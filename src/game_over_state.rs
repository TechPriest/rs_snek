use game_common::*;
use game_state::GameState;
use game_services::GameServices;
use point::Point;
use buffer::Buffer;
use input::Input;
use main_state::MainState;

pub struct GameOverState {

}

impl GameState for GameOverState {
    fn draw_once(&self, buffer: &mut Buffer) {
        const I: u32 = BLACK;
        const W: u32 = WHITE;
        const SCREEN: [u32; WIDTH as usize * HEIGHT as usize] = [
            I, I, I, I, W, W, I, I, I, I,
            I, I, I, W, I, I, W, I, I, I,
            I, I, I, W, I, I, W, I, I, I,
            I, I, I, W, I, I, W, I, I, I,
            I, I, I, I, W, W, I, I, I, I,
            I, I, I, I, I, I, I, I, I, I,
            I, I, I, W, I, I, W, I, I, I,
            I, I, I, W, I, I, W, I, I, I,
            I, I, I, I, W, W, I, I, I, I,
            I, I, I, I, I, I, I, I, I, I,
            I, I, I, W, W, W, W, I, I, I,
            I, I, I, W, I, I, I, I, I, I,
            I, I, I, W, W, I, I, I, I, I,
            I, I, I, W, W, W, W, I, I, I,
            I, I, I, I, I, I, I, I, I, I,
            I, I, I, W, W, W, W, I, I, I,
            I, I, I, W, I, I, W, I, I, I,
            I, I, I, W, W, W, W, I, I, I,
            I, I, I, W, I, W, I, I, I, I,
            I, I, I, W, I, I, W, I, I, I,
        ];

        for y in 0 .. HEIGHT  {
            for x in 0 .. WIDTH  {
                let pt = Point{x, y};
                let offset = y as usize * WIDTH as usize + x as usize;
                let clr = SCREEN[offset];
                buffer.set_point(&pt, clr);
            }
        }
    }

    fn update(&mut self, input: &Input, game_services: &mut GameServices) {
        if !input.axes().is_zero() || input.pause() {
            game_services.set_next_state(Box::new(MainState::new()));
        }
    }
}